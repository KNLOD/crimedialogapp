package com.example.crimedialogapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import com.example.crimedialogapp.model.Crime
import com.example.crimedialogapp.model.Date
import com.google.android.material.textfield.TextInputEditText

class CrimeFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container : ViewGroup?,
        savedInstance: Bundle?
        ) : View?{

        return inflater.inflate(R.layout.fragment_crime, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val crimeTitle : TextInputEditText = view.findViewById(R.id.crime_title_edit)
        val isCrimeSolved : CheckBox = view.findViewById(R.id.crime_solved_check)
        val submitCrime : Button = view.findViewById(R.id.submit_crime)

        val viewModel : CrimeDetailViewModel by activityViewModels()

        submitCrime.setOnClickListener{
            viewModel.setCrime(Crime(
                    id = 0,
                    title = crimeTitle.text.toString(),
                    date = Date(
                        day = 0,
                        month = 0,
                        year = 0
                    ),
                    isSolved = isCrimeSolved.isActivated)
            )
            dismiss()
            }
    }
}