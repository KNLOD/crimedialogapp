package com.example.crimedialogapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.crimedialogapp.model.Crime

class CrimeDetailViewModel : ViewModel(){
    private var _crime = MutableLiveData<Crime>()
    val crime : LiveData<Crime>
    get() = _crime

    fun setCrime(crime : Crime){
        _crime.value = crime
    }



}