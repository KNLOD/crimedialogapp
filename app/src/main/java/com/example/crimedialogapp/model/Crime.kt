package com.example.crimedialogapp.model


data class Crime(
    val title : String,
    val id : Long,
    private var date : Date,
    val isSolved : Boolean = false
){
    fun getDate() : Date{
        return this.date
    }
    fun setDate(date : Date){
        this.date = date
    }
}
