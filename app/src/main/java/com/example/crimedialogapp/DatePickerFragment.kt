package com.example.crimedialogapp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import com.example.crimedialogapp.model.Date
import java.util.Calendar

class DatePickerFragment : DialogFragment() {
    override fun onCreateView(
        inflater : LayoutInflater,
        container : ViewGroup?,
        savedInstanceState: Bundle?
        ) : View?{



        var rootView : View = inflater.inflate(R.layout.fragment_date_picker, container, false)

        return rootView




    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel : CrimeDetailViewModel by activityViewModels()
        val today = Calendar.getInstance()
        val datePicker : DatePicker = view.findViewById(R.id.date_picker)

        datePicker.init(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH),
            DatePicker.OnDateChangedListener { datePicker, year, month, day ->
                viewModel.crime.value?.setDate(Date(
                    year = datePicker.year,
                    month = datePicker.month,
                    day = datePicker.dayOfMonth
                )
                )
                viewModel.crime.value?.let { viewModel.setCrime(it) }
            }
        )





    }

}