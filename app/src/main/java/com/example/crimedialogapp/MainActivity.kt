package com.example.crimedialogapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import androidx.lifecycle.Observer


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val crimeTitle : TextView = findViewById(R.id.crime_text_view)
        val setDateButton : Button = findViewById(R.id.set_crime_date)
        val crimeInfromationButton : Button = findViewById(R.id.crime_infromation_button)

        val viewModel : CrimeDetailViewModel by viewModels()



        viewModel.crime.observe(this, Observer{ crime ->
            crimeTitle.text = "${crime.title} \nDate: ${crime.getDate().day}.${crime.getDate().month}.${crime.getDate().year}\nSolved -> ${crime.isSolved}"





        })

        setDateButton.setOnClickListener{
            var dialog = DatePickerFragment()
            dialog.show(supportFragmentManager, "pickDate")
        }

        crimeInfromationButton.setOnClickListener{
            var dialog = CrimeFragment()
            dialog.show(supportFragmentManager, "setCrimeInfo")
        }




    }
}